FROM golang:latest as builder

WORKDIR /src

RUN export GO111MODULE=on

COPY app/ .

RUN go mod init index

RUN go build index.go

FROM scratch

WORKDIR /

COPY --from=builder /src .

ENTRYPOINT ["./index"]
